const readlineSync = require('readline-sync');

var num = readlineSync.question("Digite um número:\n");
let resp = "";


if (num <= 25) {
    resp = 'Está dentro do intervalo [0, 25]';
} else if (num <= 50) {
    resp = 'Está dentro do intervalo (25, 50]';
} else if (num <= 75) {
    resp = 'Está dentro do intervalo (50, 75]';
} else if (num <= 100) {
    resp = 'Está dentro do intervalo (75, 100]';
} else {
    resp = 'Fora de intervalo.';
}

console.log(resp);