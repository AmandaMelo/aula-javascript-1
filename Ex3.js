const readlineSync = require('readline-sync');

var tempo = readlineSync.question("Digite o tempo de viagem em horas:\n");
var vel = readlineSync.question("Digite a velocidade do automóvel:\n");

let litros = Math.ceil((tempo * vel)/12);
console.log(`A quantidade de litros necessária é igual a ${litros}.`);