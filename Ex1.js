const readlineSync = require('readline-sync');

var seg = readlineSync.question("Diga a duração em segundos:\n");
let hora = 0;
let min = 0;

if (seg >= 3600) {
    hora = Math.floor(seg / 3600);
    seg = seg % 3600;
}
if (seg >= 60) {
    min = Math.floor(seg / 60);
    seg = seg % 60;
}
console.log(`${hora}:${min}:${seg}`);